
Copyright 2017 Ubertus.org

Description
-----------
This module makes FullCalendar rendering persistent with regards to views 
(i.e. a cookie is remembered for the user's FullCalendar view's 
defaultView and/or starting date)

This is useful when you have exposed filters for a FullCalendar view and 
would like the FullCalendar to not only retain the exposed filters (in 
the settings of the view) but would also like the FullCalendar to show
the at the same spot of the FullCalendar (Month/Week/Day view) and 
starting date.

Installation
------------
To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the entire directory and all its contents to your
   modules directory.

Configuration
-------------
To enable this module do the following:

1. Go to Admin -> Modules, and enable FullCalendar Persistent.

Author
------
Brad Duell (BDuell) (https://www.drupal.org/u/bduell)

Maintainers
-----------
Brad Duell (BDuell) (https://www.drupal.org/u/bduell)
Francois Carpentier (Francewhoa) (https://www.drupal.org/u/francewhoa)

Supporting organizations
-----------
Ubertus.org
