<?php

/**
 * @file
 * Makes FullCalendar rendering persistent with regards to views (i.e. a cookie is remembered for the user's FullCalendar view's defaultView and/or starting date).
 */

/**
 * Implements hook_fullcalendar_api().
 */
function fullcalendar_persistent_fullcalendar_api() {
  return array(
    'api' => fullcalendar_api_version(),
  );
}

/**
 * Implements hook_fullcalendar_options_info().
 */
function fullcalendar_persistent_fullcalendar_options_info() {
  return array(
    'fullcalendar_persistent' => array(
      'js' => TRUE,
      'no_fieldset' => TRUE,
      'weight' => 5,
    ),
  );
}


/**
 * Implements hook_views_pre_view().
 *
 * FullCalendar configuration setting if a cookie is set for the user's FullCalendar defaultView and/or starting date...
 */
function fullcalendar_persistent_views_pre_view(&$view, &$display_id, &$args) {
	if (isset($view->style_plugin->options['fullcalendar_options'])) {
		if (isset($_COOKIE['STYXKEY_calendarDefaultView']) || isset($_COOKIE['STYXKEY_calendarCurrentDate'])) {
			$view->init_style();
		}
		if (isset($_COOKIE['STYXKEY_calendarDefaultView'])) {
			// Set the calendarDefaultView for this view's FullCalendar...
			$view->style_plugin->options['fullcalendar_options']['defaultView'] = $_COOKIE['STYXKEY_calendarDefaultView'];
		}
		if (isset($_COOKIE['STYXKEY_calendarCurrentDate'])) {
			// Set the starting date for this view's FullCalendar...
			$view->style_plugin->options['date'] = array('year' => date("Y", strtotime($_COOKIE['STYXKEY_calendarCurrentDate'])), 'month' => ltrim(date("m", strtotime($_COOKIE['STYXKEY_calendarCurrentDate'])), '0'), 'date' => ltrim(date("d", strtotime($_COOKIE['STYXKEY_calendarCurrentDate'])), '0'));
		}
	}
}

/**
 * Implements hook_preprocess_page().
 *
 * Adds jquery to page to set cookie for calendarDefaultView and calendarCurrentDate when rendering...
 */
function fullcalendar_persistent_preprocess_page(&$vars) {
	drupal_add_js('
(function($) {
	if (typeof Drupal.fullcalendar !== "undefined") {
Drupal.fullcalendar.plugins.fullcalendar_persistent = {
  options: function (fullcalendar, settings) {
    var options = {
      viewRender: function(view, element) {
	      // Set the DefaultView for future viewings...
	      $.cookie(\'STYXKEY_calendarDefaultView\', view.name, {expires:7, path: \'/\'});
	      // Set the starting date for future viewings...
	      $.cookie(\'STYXKEY_calendarCurrentDate\', view.start.getFullYear()+\'-\'+view.start.getMonth()+\'-\'+view.start.getDate(), {expires:7, path: \'/\'});
      },
    };
    return options;
  }
};
  }
}(jQuery));
	', array('type' => 'inline'));
}

/**
 * Implements hook_module_implements_alter().
 *
 * Ensure fullcalendar_persistent runs last when hook_fullcalendar_options_pre_view is invoked.
 */
function fullcalendar_persistent_module_implements_alter(&$implementations, $hook) {
  // Testing with isset is only necessary if module doesn't implement the hook.
  if ($hook == 'preprocess_page') {
    // Move our hook implementation to the bottom.
    $group = $implementations['fullcalendar_persistent'];
    unset($implementations['fullcalendar_persistent']);
    $implementations['fullcalendar_persistent'] = $group;
  }
}
